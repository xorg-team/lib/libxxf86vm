Source: libxxf86vm
Section: x11
Priority: optional
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libx11-dev,
 x11proto-dev,
 libx11-6,
 libxext-dev,
 pkg-config,
 quilt,
 xutils-dev,
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/xorg-team/lib/libxxf86vm.git
Vcs-Browser: https://salsa.debian.org/xorg-team/lib/libxxf86vm

Package: libxxf86vm1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Multi-Arch: same
Description: X11 XFree86 video mode extension library
 libXxf86vm provides an interface to the XFree86-VidModeExtension
 extension, which allows client applications to get and set video mode
 timings in extensive detail.  It is used by the xvidtune program in
 particular.
 .
 More information about X.Org can be found at:
 <URL:http://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXxf86vm

Package: libxxf86vm1-dbg
Section: debug
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libxxf86vm1 (= ${binary:Version})
Multi-Arch: same
Description: X11 XFree86 video mode extension library (debug package)
 libXxf86vm provides an interface to the XFree86-VidModeExtension
 extension, which allows client applications to get and set video mode
 timings in extensive detail.  It is used by the xvidtune program in
 particular.
 .
 This package contains the debug versions of the library found in libxxf86vm1.
 Non-developers likely have little use for this package.
 .
 More information about X.Org can be found at:
 <URL:http://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXxf86vm

Package: libxxf86vm-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libxxf86vm1 (= ${binary:Version}),
 libx11-dev,
 x11proto-dev
Description: X11 XFree86 video mode extension library (development headers)
 libXxf86vm provides an interface to the XFree86-VidModeExtension
 extension, which allows client applications to get and set video mode
 timings in extensive detail.  It is used by the xvidtune program in
 particular.
 .
 This package contains the development headers for the library found in
 libxxf86vm1.  Non-developers likely have little use for this package.
 .
 More information about X.Org can be found at:
 <URL:http://www.X.org>
 .
 This module can be found at
 git://anongit.freedesktop.org/git/xorg/lib/libXxf86vm
